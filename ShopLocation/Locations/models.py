from django.db import models


class City(models.Model):
    name = models.CharField(max_length=50, blank=False, help_text='Название города')

    def __str__(self):
        return self.name


class Street(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=False, help_text='Название улицы')

    def __str__(self):
        return f'{City.name}, {self.name}'
