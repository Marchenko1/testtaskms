from django.db import models

from ShopLocation.Locations.models import City, Street


class Shop(models.Model):
    name = models.CharField(max_length=100, blank=False, help_text='Название магазина')
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    street = models.ForeignKey(Street, on_delete=models.CASCADE)
    house = models.CharField(max_length=10, blank=False, help_text='Номер дома')
    time_open = models.TimeField(auto_now=False, auto_now_add=False)
    time_close = models.TimeField(auto_now=False, auto_now_add=False)

    def __str__(self):
        return self.name
